import requests

'''
【第一步】
封装请求接口的函数，适合于需要校验登录权限的接口，
设置_headers为了减少再参数化用例的时候减少headers: xxx的步骤
加上**kwargs的参数为了请求接口便于传入其他参数，例如cookies= xx, files= xx
'''


# 封装requset请求的方法（做一点预处理）
def sendRequests(body, headers=None, **kwargs):
    if headers is None:
        headers = {
            "Connection": "keep-alive",
            "Content-Length": "25",
            "Accept": "application/json, text/javascript, */*; q=0.01",
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36",
            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
            "Accept-Encoding": "gzip, deflate, br",
            "Accept-Language": "zh-CN,zh;q=0.9,en;q=0.8"
        }
    return requests.request(**body, headers=headers, **kwargs)
