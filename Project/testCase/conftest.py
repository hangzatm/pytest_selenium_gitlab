import json
import requests
import pytest
# import mysql.connector
# import MySQLdb  #Python3不支持MySQLdb模块，需要用pymysql替代
import pymysql

'''
【第三步】
'''


# 登录接口，返回Cookie
@pytest.fixture(scope="class")
def login():
    headers = {
        "Connection": "keep-alive",
        "Content-Length": "25",
        "Accept": "application/json, text/javascript, */*; q=0.01",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36",
        "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "zh-CN,zh;q=0.9,en;q=0.8"
    }
    body = {
        "username": "zyj",
        "password": "111"
    }
    # data = json.dumps(body)  # 将Python对象转成JSON,请求如果不是json格式的，例如post请求入参是x-www-form-urlencoded格式的，不能使用json.dumps()处理入参
    response = requests.post("http://localhost:8080/bookrent/User/userpasswordYanZhengIfuserNameExist", body,
                             headers=headers)
    cookie = response.cookies
    print("登录情况：" + response.text)
    return cookie


# 连接数据库
@pytest.fixture(scope="class")
def connectDb():
    # db = pymysql.connect("localhost", "root", "1", "bookrent",charset="utf8""")
    db = pymysql.connect(host="localhost",
                         user="root", password="1",
                         database="bookrent",
                         charset="utf8")
    return db
