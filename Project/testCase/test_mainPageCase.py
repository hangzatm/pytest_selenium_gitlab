import yaml
import pytest
import allure
import json
from Project.testSuite import mainPageSuit

'''
【第五步】
'''

@allure.feature('慕语书屋首页')
class TestMainPageCase:
    # 注意！此时yml文件中的格式必须是mapping格式，不然不能进行**解包
    with open('D:/Jenkins/workspace/pytest-study/Project/testData/mainPageData.yml', 'r', encoding='utf-8') as file:
        data = yaml.safe_load(file)

    '''
    查询所有图书分类
    '''
    @allure.story('查询所有图书分类')
    @pytest.mark.parametrize('request_body,aa', [(data['mainPage_get_AllBookType'],data['mainPage_get_SearchBookByKeyword'])])  # 1、拿到yml文件中我们想要的数据
    @pytest.mark.skip()
    def test_get_AllBookType(self, login, connectDb,
                             request_body,aa):  # login为conftest.py中的login()登录方法,connectDb则是连接数据库的方法
        responseAllBookTypeList = []  # 接口请求得到的所有图书分类
        dbAllBookType = []  # 数据库中的所有图书分类
        # 最终目的：将接口查出的数据和数据库中的数据进行比较
        # 2、调用接口，使用testSuit中封装好的方法
        response = mainPageSuit.get_AllBookType(request_body, login)
        print("接口查到的数据【所有图书分类】是：" + str(response.text))
        resDic = json.loads(response.text)

        responseAllBookType = resDic['res']
        status = resDic['status']
        #3、将接口查出来的数据存放到列表中
        for i in responseAllBookType:
            obj = (int(i['bookTypeId']), i['bookTypeName'])
            responseAllBookTypeList.append(obj)
        try:
            # 使用 cursor() 方法创建一个游标对象 cursor
            cursor = connectDb.cursor()
            # 使用 execute()  方法执行 SQL 查询
            sql = "SELECT * FROM BOOK_TYPE"
            cursor.execute(sql)
            # 使用 fetchAll() 方法获取所有数据.
            data = cursor.fetchall()
            # 4、将数据库查出来的数据存放到列表中
            for j in data:
                dbAllBookType.append(j)
            cursor.close()
        except Exception as ex:
            raise Exception('数据库查询所有图书分类失败：' + str(ex))
        pytest.assume(response.status_code == 200, '请求失败，返回状态码为：' + str(response.status_code))
        pytest.assume(status == 202)
        print("接口获取到的数据是：" + str(responseAllBookTypeList))
        print("数据库获取到的数据是：" + str(dbAllBookType))
        pytest.assume(responseAllBookTypeList == dbAllBookType)


    '''
    根据书本关键字搜索图书
    '''
    @allure.story('根据书本关键字搜索图书')
    @pytest.mark.parametrize('request_body', data['mainPage_get_SearchBookByKeyword'])  # 1、拿到yml文件中我们想要的数据
    # @pytest.mark.skip()
    def test_get_SearchBookByKeyword(self, login, connectDb,
                             request_body):  # login为conftest.py中的login()登录方法,connectDb则是连接数据库的方法
        responseBookList = []  # 接口请求得到的图书
        dbBookList = []  # 数据库查找到的图书
        # 最终目的：将接口查出的数据和数据库中的数据进行比较
        # 2、调用接口，使用testSuit中封装好的方法
        response = mainPageSuit.get_AllBookType(request_body, login)
        # print("接口查到的数据【搜索到的图书】是：" + str(response.text))
        resDic = json.loads(response.text)

        responseAllBookType = resDic['res']
        status = resDic['status']
        for i in responseAllBookType:
            responseBookList.append(i['bookName'])
        try:
            # 使用 cursor() 方法创建一个游标对象 cursor
            cursor = connectDb.cursor()
            # 使用 execute()  方法执行 SQL 查询
            sql = "SELECT * FROM BOOK_INFO WHERE BOOK_NAME LIKE '%"+ str(request_body['data']['keyword'])+"%'"
            cursor.execute(sql)
            # 使用 fetchAll() 方法获取所有数据.
            data = cursor.fetchall()
            for j in data:
                dbBookList.append(j[2])
            cursor.close()
        except Exception as ex:
            raise Exception('数据库根据书本关键字搜索图书失败：' + str(ex))
        pytest.assume(response.status_code == 200, '请求失败，返回状态码为：' + str(response.status_code))
        pytest.assume(status == 202)
        print("接口获取到的数据是：" + str(responseBookList))
        print("数据库获取到的数据是：" + str(dbBookList))
        pytest.assume(responseBookList == dbBookList)