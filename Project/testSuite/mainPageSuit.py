from Project.baseAPI.baseAPI import sendRequests

'''
【第二步】
 定义需要测试的接口
 request_body通过testData.mainPageCase.yml中获取，主要获取 method,url和json入参
 cookie 通过testCase.conftest.py 下的login接口获取
'''

#查询所有图书分类
def get_AllBookType(request_body, cookie):
    response = sendRequests(request_body, cookies=cookie)
    return response

#根据书本关键字搜索图书
def get_SearchBookByKeyword(request_body, cookie):
    response = sendRequests(request_body, cookies=cookie)
    return response