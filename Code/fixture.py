import pytest
# test_fixture1.py

@pytest.fixture(scope='module')
def test1():
    print('\n开始执行module')


@pytest.fixture(scope='class')
def test2():
    print('\n开始执行class')


@pytest.fixture(scope='function')
def test3():
    print('\n开始执行function')

@pytest.fixture(scope='function',autouse= True)
def test5():
    print('\n开始执行function2')


def test_a(test1):
    print('---用例a执行---')


def test_d(test2,test3):
    print('---用例d执行---')


class TestCase:

    def test_b(self,test3):
        print('---用例b执行---')

    def test_c(self):
        print('---用例c执行---')


if __name__ == '__main__':
    pytest.main(['-s', 'test_fixture1.py'])